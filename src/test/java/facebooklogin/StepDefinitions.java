package facebooklogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class StepDefinitions {

    @Given("^Mở trang đăng nhập facebook$")
    public void mo_trang_dang_nhap_facebook() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver"); // trỏ đến folder chứa chromedriver
        WebDriver driver = new ChromeDriver();   // khởi tạo chromedriver
        driver.get("https://www.facebook.com/"); // open browser trang web cần auto test
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); // wait 30s cho các element trên page dk load lên
        driver.manage().window().maximize(); // mở full màn hình
    }

    @When("^user đăng nhập với email là \"([^\"]*)\" và password là \"([^\"]*)\"$")
    public void user_dang_nhap_voi_email_va_password(String email, String password) {
        // viết code để implement step tương ứng
    }

    @Then("^Hiển thị trang chủ của màn hình facebook$")
    public void hien_thi_trang_chu_cua_man_hinh_facebook() {
    }


//    @Given("^Mở màn hình đăng nhập facebook$")
//    public void mo_man_hinh_dang_nhap_facebook() {
//    }
//
//    @When("^user đăng nhập với (.+) và (.+)$")
//    public void user_dang_nhap_voi_email_password(String email, String password) {
//    }
//
//    @Then("^Hiển thị (.+) cho người dùng$")
//    public void hien_thi_message_cho_nguoi_dung(String message) {
//    }

}
