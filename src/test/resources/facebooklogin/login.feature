#- Đây là file định nghĩa các Test Case, có đuôi .feature
#- Để viết TC, cucumber định nghĩa ra các từ khóa: Feature, Scenario, Scenario Outline, Example,
#  Given, When, Then, And, But, Background
#+ ) Feature: mô tả overview về chức năng cần automation test
#+ ) Scenario: mô tả trường hợp test
#+ ) Scenario Outline, Example: 2 từ khoá này thường đi cặp với nhau, mô tả những TC có cùng các step chỉ khác nhau về dữ liệu
#+ ) Given, When, Then, And, But: các từ mô tả các step thực hiện trong 1 TC
#+ ) Background: Trường hợp các TC trong 1 file feature có chung 1 vài step thì có thể viết 1 lần bằng cách dùng từ khóa Background
#Lưu ý: Sau 1 số từ khóa có dấu ":"

Feature: Đăng nhập facebook

  Scenario: Đăng nhập thành công với email và password hợp lệ
    Given Mở trang đăng nhập facebook
    When user đăng nhập với email là "thuy74809@gmail.com" và password là "123abc"
    Then Hiển thị trang chủ của màn hình facebook

  Scenario Outline: Đăng nhập thất bại với email và password không hợp lệ
    Given Mở màn hình đăng nhập facebook
    When user đăng nhập với <email> và <password>
    Then Hiển thị <message> cho người dùng
    Examples:
      | email              | password  | message            |
      |thuy@gmail.com      |12345      |email không đúng    |
      |thuy                |123456     |email sai định dạng |
      |thuy74809@gmail.com |123abcde   |password không đúng |